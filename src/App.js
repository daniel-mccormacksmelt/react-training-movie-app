import React, { Component } from 'react';
import Movie from './components/Movie/Movie'
import Title from './components/Title/Title'
import classes from './App.module.css';
import axios from 'axios'

class App extends Component {
  state = {
    favouriteMovies: [
      {title: 'Arrival'},
      {title: 'Star Wars'},
      {title: 'Airplane!'},
      {title: 'Dredd'},
      {title: 'Mad Max: Fury Road'}
    ],
    movies: [

    ]
  }

  componentDidMount() {
    for(let i = 0; i < this.state.favouriteMovies.length; i++){
      axios.get(`http://www.omdbapi.com/?apikey=8211045&t=${this.state.favouriteMovies[i].title}`)
      .then(response => {
        const movies = this.state.movies
        movies.push(response.data)
        this.setState({movies: movies})
      })  
    }
  } 
  
  render() {
    const movies = this.state.movies.map((movie) => {
        return <Movie
        title={movie.Title}
        img={movie.Poster}
        director={movie.Director}
        runtime={movie.Runtime}
        key={movie.Website} />
    })

    return (
      <div className={classes.App}>
        <Title>
            Favourite Movies
        </Title>
        <div className={classes.movieGrid}>
          {movies}
        </div>
      </div>
    );
  }
}

export default App;
