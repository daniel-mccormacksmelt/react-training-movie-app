import React from 'react'
import MovieInfo from './MovieInfo/MovieInfo'

import classes from '../../styles/components/Movie/Movie.module.css'

const movie = (props) => {
  const background = {
    backgroundImage: 
      `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url(${props.img})`
  }
  return (
    <div style={background} className={classes.Movie}>
      <h2>{props.title}</h2>
      <MovieInfo director={props.director} runtime={props.runtime} />
    </div>
  )
}

export default movie