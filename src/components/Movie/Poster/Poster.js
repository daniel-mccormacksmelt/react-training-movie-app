import React from 'react'
import classes from '../../../styles/components/Movie/Poster/Poster.module.css'

const poster = (props) => {
  return (
    <img className={classes.Poster} src={props.src} alt={props.alt} />
  )
}

export default poster