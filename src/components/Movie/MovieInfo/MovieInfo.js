import React, {Fragment} from 'react'

const movieInfo = (props) => {
  return (
    <Fragment>
      <h3>{props.director}</h3>
      <p>{props.runtime}</p>
    </Fragment>
  )
}

export default movieInfo