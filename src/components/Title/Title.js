import React, {Fragment} from 'react'
import classes from '../../styles/components/Title/Title.module.css'

const title = (props) => {
  return (
    <div className={classes.Title}>
      <h1>{props.children}</h1>
      <hr />
    </div>
  )
}

export default title